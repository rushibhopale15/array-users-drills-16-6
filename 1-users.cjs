const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/


//Q1 Find all users who are interested in playing video games.

function findAllIntrestedInVideoGames(users) {
    let interestedInVideoGames = Object.keys(users)
        .filter((user) => {
            let interestArea = users[user].interests
            if (interestArea === undefined) {
                interestArea = users[user].interest
            }
            interestArea = interestArea.join(' ');
            //console.log(interestArea);
            return interestArea.includes("Video Games");

        })
    console.log(interestedInVideoGames);
}
findAllIntrestedInVideoGames(users);


//Q2 Find all users staying in Germany.

function findUserInGermany(users) {

    let germanyUser = Object.keys(users)
        .filter((user) => {
            return users[user].nationality === "Germany";
        })
    console.log(germanyUser);
}
//findUserInGermany(users);

/*Q3 Sort users based on their seniority level 
for Designation - Senior Developer > Developer > Intern
for Age - 20 > 10 */

function sortUsers(users) {
    
    let sortedByAgeData = Object.keys(users).sort((user1, user2) => {
        return users[user1].age - users[user2].age
    })
    console.log(sortedByAgeData);
}
//sortUsers(users);


//Q4 Find all users with masters Degree.

function findUserWithMasters(users) {

    let mastersUser = Object.keys(users)
        .filter((user) => {
            return users[user].qualification === "Masters";
        })
    console.log(mastersUser);
}
//findUserWithMasters(users);


//Q5 Group users based on their Programming language mentioned in their designation.

function makeUserGroup(users) {

    let userArray = Object.keys(users);

    userArray.reduce((groupOfUser,user) => {
            let desgination = users[user].desgination;
            
        },{})
    console.log(userArray);
}
//makeUserGroup(users);